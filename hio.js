$(document).ready(function() {

// jQUERY TILL SIDAN KURSINFORMATION

// Hämtar id courseinfo och lägger till <th> under tbody vilket visar rubrikerna på informationen som sedan hämtas från APIt.
$("#courseinfo > tbody").append("<th>Kurs ID</th>" + "<th>Kursnamn</th>" + "<th>Antal poäng</th>" + "<th>Skola</th>" + "<th>Startar vecka</th>" + "<th>Slutar vecka</th>" + "<th>Lärare</th>");

    $.get("http://webbred2.utb.hb.se/~fewe/api/api.php?data=courses", data => { // Hämtar APIt med kursinformation (kursid, kursnamn, kurspoäng, skola osv.) och visar det som en htmltabell under tbody. 
          data.forEach(course => {
          $("#courseinfo > tbody").append("<tr><td>" + course.courseId + "</td><td>" + course.courseName + "</td><td>" + course.credit + "</td><td>" + course.school + "</td><td>" + course.startWeek + "</td><td>" + course.endWeek + "</td><td>" + course.teachers +"</td></tr>");
      });
    });
  


// jQUERY TILL QUIZSIDAN

$.get("http://webbred2.utb.hb.se/~fewe/api/api.php?data=quiz", function (data, status) { //Hämtar data från APIt quiz
console.log(data);
dataset = data; // Sparar datan som hämtas via APIt

for (var i=0; i<data.length; i++) {
  var questionElement = $('<h3>'); //Hämtar värdet question från API och lägger till det i html-strukuten som en <h3>
  questionElement.html(data[i].question);

  var section = $('section'); // Gör så att det som hämtas ovan skrivs ut under section i body.
  section.append(questionElement);

for (var k=0; k<data[i].incorrect_answers.length; k++) { // For loop som hämtar de felaktiga svaren i APIt
  var incorrectAnswersElement = $('<p class="incorrectAnswers">'); // Hämtar de felaktiga svaren och skriver ut på sidan via <p> element.
  incorrectAnswersElement.html(data[i].incorrect_answers[k]);
         
  var radioButtons = $('<p class="radiobutton">'); // Skapar p-element för knapp till varje felaktigt svar.
  radioButtons.append('<input type="radio" name="'+ i + 'm'+'" value="' + data[i].incorrect_answers[k] + '"/>'); //Lägger till knappen under felaktiga svar.
  // name="'+ i + 'm'+'" gör att varje grupp svar får unika namn.

  section.append(incorrectAnswersElement); // Skriver ut de felaktiga svaren på sidan.
  incorrectAnswersElement.append(radioButtons); // Skriver ut knapparna för varje svar.
  };

  var correctAnswerElement = $('<p class="correctAnswers">');  // Hämtar de korrekta svaren och skriver ut tillsammans med de felaktiga svaren.
  correctAnswerElement.html(data[i].correct_answer);
  section.append(correctAnswerElement); // Gör så att rätt svar visas på sidan

// Följande kod lägger till en radioButton på alla <p>-taggar för rätt svar, variablerna i "name" hämtas från i samt m för correctAnswer
//  och skapar unika input name, detta för att man ska kunna klicka i en box per fråga.
var radioButtons = $('<p class="radiobutton">');
  radioButtons.append('<input type="radio" name="'+ i + 'm' +'" value= "' + data[i].correct_answer + '"/>');
  correctAnswerElement.append(radioButtons);
    };
});

    $("div.quizbutton").append( //Hämtar divelement class quizbutton och skapar en knapp för att kunna visa quizets rätta svar
        $("<input/>", {
            type: 'submit',
            class: 'rightAnswers',
            value: 'Se rätt svar'
}));


$("input[class=rightAnswers]").click(function(){ // Hämtar class rightAnswers som skapades ovan
$('.correctAnswers').append(' &#10004;').css("color", "green"); //Hämtar de rätta svaren och ändrar svaret till grön samt lägger till en check-symbol
});



// jQUERY SOM APPLICERAS PÅ MER ÄN EN AV SIDORNA

$(".copyright").append("<p>&copy; Högskolan i Oskarshamn 2020</p>")  // Hämtar class copyrigt från html-koden och lägger till en copyrightsymbol tillsammans med skolans namn och år

// Koden nedan hämtar nav och den sidan man är inne på aktiver .active i csskoden och gör länknamnet fetstil.
// Detta för att man tydligare ska se vilken sida man är inne på.
$('.nav a').filter(function(){return this.href==location.href}).parent().addClass('active').siblings().removeClass('active')
		$('.nav a').click(function(){
			$(this).parent().addClass('active').siblings().removeClass('active')	
		})
	}) 