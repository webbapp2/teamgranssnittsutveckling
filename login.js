// jQUERY TILL INLOGGNINGSSIDAN

$(document).ready(function() {

    $(".login-form").submit(function(event){ //Hämtar class login-form från filen login.html och följande function ska köras vid klick på formulärets knapp "submit"
    event.preventDefault();
    $.get("http://webbred2.utb.hb.se/~fewe/api/api.php?data=students", function (data, status) { //Hämtar APIt students
    
    var checkLogin = false; //Skapar variabel checkLogin och sätter värdet false
    for (var i=0; i<data.length; i++) { 
        //Om username i APIt är samma som det som fyllts i fältet med id usernam och om password i APIt är samma som det som fyllts i fältet
        if (data[i].login.username === $('#username').val() && data[i].login.password === $('#password').val()) 
          checkLogin = true; //Så är inloggningen sann//lyckad
    
    }
    
    if(checkLogin){ //Om checkLogin stämmer dvs, inloggningen är lyckad, ska användaren pekas om till sidan courseinfo.html
        window.location = 'courseinfo.html';
    }else{
        alert('Inloggningen misslyckades') //Om checkLogin inte stämmer dvs, inloggningen är misslyckad ska användaren se en alertruta med "Inloggningen misslyckades"
    }
    
    });
    });
});